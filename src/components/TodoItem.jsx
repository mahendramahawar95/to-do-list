import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { deleteTodo } from '../redux/actions';

function TodoItem({ todo }) {
    const [editable, setEditable] = useState("line")
 
    let dispatch = useDispatch();

    return (
        <div>
            <div className="row mx-2 align-items-center">
        
                <div className="col width-h">
                
                        <h4  onClick={() => {
                        if(editable === "black line"){
                            setEditable("")
                        }else{
                            editable === "green line" ? 
                            setEditable("black line"):
                        setEditable("green line");
                        }
                       
                        
                      }} 
                      className={editable}>{todo.name}</h4>
                </div>
             
                <button className="btn btn-danger m-2"
                    onClick={() => dispatch(deleteTodo(todo.id))}
                >Delete</button>
            </div>
        </div>
    )
}

export default TodoItem
