import React from 'react'
import TodoItem from './TodoItem'
import { useSelector } from 'react-redux'
import TodoInput from '../components/TodoInput'

function TodoList() {
    let todos = useSelector(state=>state);

    return (
        <div className="card">
           
        <div className="my-4">
            {todos.map((todo)=>   {
                return <TodoItem key={todos.id} todo={todo}/>;
            })}
        </div>
        <TodoInput/>
        </div>
    )
}

export default TodoList
